<?php

class ExerciseString
{
    public $check1;
    public $check2;

    public function readFile($fileName)
    {
        $file = fopen($fileName, 'r');
        if ($file) {
            $str = fread($file, filesize($fileName));
            fclose($file);
            return $str;
        } else {
            echo 'Loi doc file';
        }
    }
	
    public function checkValidString($str)
    {
        $indexRest = strpos($str, 'restaurant');
        $indexBook = strpos($str, 'book');
        if (!is_numeric($indexRest) && is_numeric($indexBook)) {
            return true;
        } elseif (is_numeric($indexRest) && !is_numeric($indexBook)) {
            return true;
        } else {
            return false;
        }
    }
	
    public function writeFile($fileName, $data)
    {
        $file = fopen($fileName, 'w');
        if ($file) {
            fwrite($file, $data);
            fclose($file);
        } else {
            $file = fopen($fileName, 'x');
            fwrite($file, $data);
            fclose($file);
        }
    }
}

$object1 = new ExerciseString();
$str1 = $object1->readFile('file1.txt');
$str2 = $object1->readFile('file2.txt');
$countStr2 = substr_count($str2, '.');
$data = '';
if ($object1->checkValidString($str1)) {
    $data = "Hop le\n";
} else {
    $data = "Khong hop le\n";
}
if ($object1->checkValidString($str2)) {
    $data .= "Hop le. Chuoi co $countStr2 cau\n";
} else {
    $data .= "Khong hop le. Chuoi co $countStr2 cau\n";
}
$object1->writeFile('result_file.txt', $data);
