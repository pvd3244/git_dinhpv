<?php

function validateString($str)
{
    $indexRest = strpos($str, 'restaurant');
    $indexBook = strpos($str, 'book');
    if (!is_numeric($indexRest) && is_numeric($indexBook)) {
        return true;
    } elseif (is_numeric($indexRest) && !is_numeric($indexBook)) {
        return true;
    } else {
        return false;
    }
}

function checkFile($fileName, $mode)
{
    $file = fopen($fileName, $mode);
    if ($file) {
        $str = fread($file, filesize($fileName));
        $count = substr_count($str, '.');
        if (validateString($str)) {
            echo "Chuoi hop le. Chuoi bao gom $count cau.";
        } else {
            echo 'Chuoi khong hop le';
        }
        fclose($file);
    }
    else {
        echo 'Open file failed';
    }
}

checkFile('file1.txt', 'r');
echo '<br>';
checkFile('file2.txt', 'r');
