<?php

abstract class Country
{
    protected $slogan;
	
    abstract public function sayHello();
	
    public function setSlogan($slogan)
    {
        $this->slogan = $slogan;
    }
}

interface Boss
{
    public function checkValidSlogan();
}

trait Active
{
    public function defindYourSelf()
    {
        return get_class($this);
    }
}

class EnglandCountry extends Country implements Boss
{
    use Active;

    public function sayHello()
    {
        echo 'Hello<br>';
    }
	
    public function checkValidSlogan()
    {
        $str = strtolower($this->slogan);
        $indexEngland = strpos($str, 'england');
        $indexEnglish = strpos($str, 'english');
        if (!is_numeric($indexEngland) && is_numeric($indexEnglish)) {
            return true;
        } elseif (is_numeric($indexEngland) && !is_numeric($indexEnglish)) {
            return true;
        } else {
            return false;
        }
    }
}

class VietNamCountry extends Country implements Boss
{
    use Active;

    public function sayHello()
    {
        echo 'Xin chao<br>';
    }
	
    public function checkValidSlogan()
    {
        $str = strtolower($this->slogan);
        $indexVietnam = strpos($str, 'vietnam');
        $indexHust = strpos($str, 'hust');
        if (is_numeric($indexVietnam) && is_numeric($indexHust)) {
            return true;
        }
        return false;
    }
}

$englandCountry = new EnglandCountry();
$vietnamCountry = new VietnamCountry();
$englandCountry->setSlogan('England is a country that is part of the United Kingdom. 
It shares land borders with Wales to the west and Scotland to the north. 
The Irish Sea lies west of England and the Celtic Sea to the southwest.');
$vietnamCountry->setSlogan('Vietnam is the easternmost country on the Indochina Peninsula. 
With an estimated 94.6 million inhabitants as of 2016, 
it is the 15th most populous country in the world.');
$englandCountry->sayHello(); // Hello
$vietnamCountry->sayHello(); // Xin chao
var_dump($englandCountry->checkValidSlogan()); // true
echo "<br>";
var_dump($vietnamCountry->checkValidSlogan()); // false
echo "<br>";
echo 'I am ' . $englandCountry->defindYourSelf(); 
echo "<br>";
echo 'I am ' . $vietnamCountry->defindYourSelf();
