use sqlbasic;
select*from `categories`;
select*from `items`;

-- Cau 1
select categories.`id`, categories.`name`, `description`, count(items.`id`)
from categories left join items
on categories.`id` = items.`category_id`
group by categories.`id`, categories.`name`, `description`;
-- Cau 2
select categories.`id`, categories.`name`, `description`, sum(`amount`)
from categories left join items
on categories.`id` = items.`category_id`
group by categories.`id`, categories.`name`, `description`;
-- Cau 3
select distinct categories.`id`, categories.`name`, `description`
from categories inner join items
on categories.`id` = items.`category_id`
where `amount` > 40;
-- Cau 4
SET SQL_SAFE_UPDATES = 0;
DELETE FROM categories
WHERE categories.`id` NOT IN (
    SELECT cid FROM (
        SELECT DISTINCT categories.`id` AS cid FROM categories INNER JOIN items ON categories.`id`=`category_id`
    ) AS c
);
